# OpenBeamPath (OBP)
OBP is a data format to describe melt sequences in a EPBF based 3d-printer.  It is based on protobuf as described in the OBP.proto file. 


# License
Copyright © 2018, 2019, 2020 Freemelt AB <opensource@freemelt.com>

OpenBeamPath is under [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)